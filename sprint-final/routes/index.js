import fruitController from "./fruits";
import userController from "./users";

export const registerRoutes = (app) => {
  fruitController(app);
  userController(app);
};
