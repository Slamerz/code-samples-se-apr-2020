import express from "express";
import { getId } from "../helpers";

export default function fruitsController(app) {
  const router = express.Router();

  router.get("/", (req, res) => {
    res.send(["apple", "orange"]);
  });

  router.post("/", (req, res) => {
    if (!req.body.name) {
      res.status(400).send("Bad data supplied to fruits dude");
      return;
    }

    const newFruit = {
      id: getId(),
      name: req.body.name,
    };

    fruits.push(newFruit);

    res.status(201).send(newFruit);
  });

  router.put("/:fruitId", (req, res) => {
    const indexOfFruit = fruits.findIndex((fruit) => fruit.id === req.params.fruitId);
    const oldId = fruits[indexOfFruit].id;
    fruits[indexOfFruit] = {
      ...req.body,
      id: oldId,
    };
    res.send("ok!");
  });

  app.use("/fruits", router);
}
