import React from "react";

const defaultState = {
  firstName: "",
  lastName: "",
  height: "medium",
};
export default class Form extends React.Component {
  state = {
    ...defaultState,
  };

  handleChange = (ev) => {
    // this.setState({ [ev.target.name]: ev.target.value });
    // const nextState = { [ev.target.name]: ev.target.value }
    ev.persist();
    this.setState((state) => {
      return {
        [ev.target.name]: ev.target.value,
      };
    });
  };

  handleSubmit = (ev) => {
    ev.preventDefault();
    this.props.addUser(this.state);
    this.setState(() => ({
      ...defaultState,
    }));
    // alert(JSON.stringify(this.state));
  };

  render() {
    const { firstName, lastName, height } = this.state;
    return (
      <>
        <form onSubmit={this.handleSubmit}>
          {/* <input name="something" placeholder="Something" /> */}
          <input name="firstName" value={firstName} placeholder="First Name" onChange={this.handleChange} />
          <input name="lastName" value={lastName} placeholder="Last Name" onChange={this.handleChange} />
          <select name="height" value={height} onChange={this.handleChange}>
            <option value="short">Short</option>
            <option value="medium">Medium</option>
            <option value="tall">Tall</option>
          </select>
          <button type="submit">Submit</button>
        </form>
      </>
    );
  }
}
