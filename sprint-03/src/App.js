import React, { useState } from "react";
import "./App.css";

const people = ["Tala", "Tj"];

const [whateverIwangt, somethingElse] = people;

console.log({
  whateverIwangt,
  somethingElse,
});

function App() {
  const [person, setPerson] = useState("");
  const [count, setCount] = useState(0);
  const [people, setPeople] = useState([]);
  // const [] = useState({
  //   firstName: "Tala",
  //   lastName: "Hawari",
  // });

  const increment = () => {
    setCount((c) => {
      return c + 1;
    });
  };

  // const decrement = () => setCount((c) => c - 1);

  function itDoesntMatterSantiago() {
    return setCount((c) => c - 1);
  }
  const setCountTo2000 = () => setCount(2000);

  const handlePersonChange = (ev) => setPerson(ev.target.value);

  const addPerson = () => {
    setPeople((p) => [person, ...p]);
    setPerson("");
    setCountTo2000();
  };

  return (
    <div className="App">
      {`My name is ${person} and I have ${count} apples.`}
      <button onClick={increment}>Increment</button>
      <button onClick={itDoesntMatterSantiago}>Decrement</button>
      <button onClick={setCountTo2000}>Set it to 2000</button>
      <br />
      <input name="person" value={person} onChange={handlePersonChange} />
      <button onClick={addPerson}>Add Person</button>
      <ul>
        {people.map((p, i) => (
          <li key={i}>{p}</li>
        ))}
      </ul>
    </div>
  );
}

export default App;
