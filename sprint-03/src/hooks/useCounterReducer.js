import { useReducer } from "react";

const reducer = (state, action) => {
  switch (action.type) {
    case "increment":
      return state + 1;
    case "decrement":
      return state - 1;
    case "multiply":
      return state * action.payload;
    default:
      return state;
    // throw new Error(`Action ${action.type} does not exist`);
  }
};

const useCounterReducer = (initialState = 0) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  return [state, dispatch];
};

export default useCounterReducer;
