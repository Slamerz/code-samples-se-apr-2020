import React, { useReducer, createContext } from "react";
const SWAPI_URL = "https://swapi.dev/api";

const SET_ROOT = "SET_ROOT";
const SET_ERROR = "SET_ERROR";
const SET_PEOPLE = "SET_PEOPLE";
const SET_LOADING = "SET_LOADING";

const sleep = (ms = 2) => new Promise((resolve) => setTimeout(resolve, ms * 1000));

const fetchResource = async (uri) => {
  await sleep(1);
  const res = await fetch(`${SWAPI_URL}${uri}`);
  return await res.json();
};

const reducer = (state, action) => {
  switch (action.type) {
    case SET_LOADING:
      return {
        ...state,
        loaders: {
          ...state.loaders,
          [action.payload.loader]: action.payload.value,
        },
      };
    case SET_ROOT:
      return {
        ...state,
        root: action.payload,
      };

    case SET_PEOPLE:
      return {
        ...state,
        people: action.payload,
      };

    case SET_ERROR:
      return {
        ...state,
        error: action.payload,
      };
    default:
      return state;
  }
};

const initialState = {
  loaders: {
    root: false,
    people: false,
  },
  root: [],
  people: [], // TODO: arrays kinda suck for data you will see why
};

const useSwapi = () => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const fetchr = async (key, uri) => {
    try {
      dispatch({ type: SET_LOADING, payload: { loader: key, value: true } });
      return await fetchResource(uri);
    } catch {
      dispatch({ type: SET_ERROR, payload: `unable to fetch ${key}` });
    } finally {
      dispatch({ type: SET_LOADING, payload: { loader: key, value: false } });
    }
    return null;
  };

  const fetchRoot = () => {
    fetchr("root", "/").then((data) => {
      if (!data) return;
      dispatch({ type: SET_ROOT, payload: Object.keys(data) });
    });
  };

  const fetchPeople = () => {
    fetchr("people", "/people").then((data) => {
      if (!data) return;
      dispatch({ type: SET_PEOPLE, payload: data.results });
    });
  };

  return [
    state,
    {
      fetchRoot,
      fetchPeople,
    },
  ];
};

// another way
// const { Provider: SwapiProvider, Consumer: SwapiConsumer } = createContext();

export const SwapiContext = createContext();

export const SwapiProvider = ({ children }) => {
  const value = useSwapi();
  return (
    <>
      <SwapiContext.Provider value={value}>{children}</SwapiContext.Provider>
    </>
  );
};

export default useSwapi;
