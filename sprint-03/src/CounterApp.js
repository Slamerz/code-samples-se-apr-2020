import React from "react";
import "./App.css";
import Counter from "./Counter";
import { CounterContextProvider } from "./counterContext";

function CounterApp() {
  return (
    <CounterContextProvider>
      <div className="App">
        <Counter />
        <Counter />
      </div>
    </CounterContextProvider>
  );
}

export default CounterApp;
