import React from "react";
import ReactLoading from "react-loading";

const Loader = ({ type = "cylon" }) => <ReactLoading color="magenta" type={type} />;

export default Loader;
