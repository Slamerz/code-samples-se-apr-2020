import React, { useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import { SwapiContext } from "../hooks/useSwapi";
import Loader from "./Loader";

const People = () => {
  const [state, actions] = useContext(SwapiContext);

  useEffect(() => {
    actions.fetchPeople();
  }, []);

  if (state.loaders.people) {
    return <Loader />;
  }

  console.log("PeopleComponent", state.root);

  return (
    <>
      <h1>People dude</h1>
      <ul>
        {state.people.map((person) => (
          <li key={person.name}>
            <Link key={person.name} to={`/${person.name}`}>
              {person.name}
            </Link>
          </li>
        ))}
      </ul>
    </>
  );
};

export default People;
