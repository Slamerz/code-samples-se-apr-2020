import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import "./index.css";
import Navigation from "./swapi/Navigation";
import People from "./swapi/People";
import { SwapiProvider } from "./hooks/useSwapi";

const Welcome = () => <h1>Welcome to SWAPI bruh!</h1>;
const NotFound = () => <h1>You're in a galaxy wayy wayy far away</h1>;

const Swapi = () => {
  return (
    <div className="App">
      <SwapiProvider>
        <BrowserRouter>
          <Navigation />
          <Switch>
            <Route path="/" exact component={Welcome} />
            <Route path="/people" component={People} />
            <Route path="*" component={NotFound} />
          </Switch>
        </BrowserRouter>
      </SwapiProvider>
    </div>
  );
};

export default Swapi;
