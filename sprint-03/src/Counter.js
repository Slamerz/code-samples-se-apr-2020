import React, { useContext } from "react";
// import useCounter from "./hooks/useCounter";
import { CounterContext } from "./counterContext";

const Counter = ({ goUpBy }) => {
  const { count, increment, decrement } = useContext(CounterContext);
  return (
    <>
      <p>{count}</p>
      <button onClick={increment}>Increment</button>
      <button onClick={decrement}>Decrement</button>
    </>
  );
};

export default Counter;
