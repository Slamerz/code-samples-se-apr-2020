import React from "react";
import useCounterReducer from "./hooks/useCounterReducer";

const CounterReducerExample = () => {
  const [state, dispatch] = useCounterReducer(1);

  const increment = () => dispatch({ type: "increment" });
  const decrement = () => dispatch({ type: "decrement" });
  const multiplyBy = () => dispatch({ type: "multiply", payload: 2 });

  return (
    <>
      <p>{state}</p>
      <button onClick={increment}>Increment</button>
      <button onClick={decrement}>Decrement</button>
      <button onClick={multiplyBy}>Multiply By</button>
    </>
  );
};

export default CounterReducerExample;
