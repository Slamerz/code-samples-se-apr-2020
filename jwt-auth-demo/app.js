import express from 'express'
import {registerRoutes} from "./routes";
import mongoose from 'mongoose'

mongoose.Promise = global.Promise;
const {EXPRESS_PORT, MONGO_CONNECTION_URL, MONGO_CONNECTION_PORT, MONGO_ADMIN_PASSWORD, MONGO_ADMIN_USERNAME}  = process.env


const dbConnectionString = `mongodb://${MONGO_ADMIN_USERNAME}:${MONGO_ADMIN_PASSWORD}@${MONGO_CONNECTION_URL}:${MONGO_CONNECTION_PORT}`

const startServer = async () =>{
    await mongoose.connect(dbConnectionString, {useNewUrlParser: true, useUnifiedTopology: true})

    const db = mongoose.connection

    db.once('open', () => {'Connected to MongoDB'})

    const app = express()

    //Middleware
    app.use(express.json()) //Tell Express how to handle incoming json


    app.get('/', (req, res) => {
        res.json({ message: "You got this response because the server is running" });
    })

    registerRoutes(app)

    app.listen(EXPRESS_PORT, () => {
        console.log(`Server successfully running on http://localhost:${EXPRESS_PORT}`);
    });
}


try {
    startServer();
} catch (err) {
    console.error("Server was unable to start");
}
