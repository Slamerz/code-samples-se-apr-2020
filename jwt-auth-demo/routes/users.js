import express from 'express'
import {User} from "../models/users";
import jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt'

export default function userRoutes(app){
    const router = express.Router();

    router.post('/new', async (req, res) =>{
       try {
           const password = await bcrypt.hash(req.body.password, 10)
           const user = new User({
               username: req.body.username,
               email: req.body.email,
               password: password,
               role: "user"
           })
           await user.save()

           res.send(user)
       }catch (e) {
           res.status(400)
           res.send(e)
       }
    })

    router.post('/login', async (req, res)=>{
        try
        {
            const user = await User.findOne({email: req.body.email})

            const isPasswordTheSame = await bcrypt.compare(req.body.password, user.password)
            if(isPasswordTheSame){
                const token = jwt.sign({email: user.email, role: user.role}, process.env.JWT_SECRET, )
                res.send({token})
            }
        }
        catch (e) {
            res.status(500)
            res.send(e)
        }
    })

    app.use('/users', router)
}
