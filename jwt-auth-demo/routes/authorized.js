import express from 'express'

export default function authorizedRoutes(app){
    const router = express.Router()
    router.get('/', (req, res)=>{
        res.send(req.loggedInUser)
    })

    app.use('/auth', router)
}
