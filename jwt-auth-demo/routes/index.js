import userRoutes from "./users";
import authorizedRoutes from "./authorized";
import jwt from 'jsonwebtoken'
import {User} from "../models/users";

export const registerRoutes = (app) => {
    userRoutes(app)
    app.use(async (req, res, next) =>{
        const incomingToken = req.headers['authorization']
        if(!incomingToken){
            next('No Token Provided');
        }
        try{
            const jwtUserData = await jwt.verify(incomingToken, process.env.JWT_SECRET)
            const user = await User.findOne({email: jwtUserData.email})
            if(!user){
                throw new Error('No User Found')
            }
            req.loggedInUser = user
            next();
        }catch (e) {
            res.status(401)
            res.send(e);
        }
    })
    authorizedRoutes(app)
};


