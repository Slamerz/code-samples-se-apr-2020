import React from "react";
import { useDispatch } from "react-redux";
import { useFormik } from "formik";
import { actions } from "../../redux/actions/auth";
import * as Yup from "yup";
import "./RegisterForm.css";

export const RegisterForm = ({ login }) => {
  const dispatch = useDispatch();

  const formik = useFormik({
    initialValues: {
      username: "",
      password: "",
      displayName: "",
    },
    validationSchema: Yup.object({
      username: Yup.string()
        .max(20, "Username must be 20 characters or less")
        .min(3, "Username must be 3 characters or more")
        .required("Required"),
      displayName: Yup.string()
        .max(20, "Display Name must be 20 characters or less")
        .min(3, "Display Name must be 3 characters or more")
        .required("Required"),
      password: Yup.string()
        .max(10, "Password must be 10 characters or less")
        .min(4, "Password must be 4 characters or more")
        .required("Required"),
    }),
    onSubmit: (values) => {
      dispatch(actions.register(values));
    },
  });

  return (
    <>
      <form id="register-form" onSubmit={formik.handleSubmit}>
        <label htmlFor="username">Username</label>
        <input type="text" autoFocus {...formik.getFieldProps("username")} />
        {formik.touched.username && formik.errors.username ? (
          <span style={{ color: "red" }}>{formik.errors.username}</span>
        ) : null}

        <label htmlFor="password">Password</label>
        <input type="password" {...formik.getFieldProps("password")} />
        {formik.touched.password && formik.errors.password ? (
          <span style={{ color: "red" }}>{formik.errors.password}</span>
        ) : null}

        <label htmlFor="displayName">Display Name</label>
        <input type="displayName" {...formik.getFieldProps("displayName")} />
        {formik.touched.displayName && formik.errors.displayName ? (
          <span style={{ color: "red" }}>{formik.errors.displayName}</span>
        ) : null}

        <button type="submit" disabled={formik.isSubmitting}>
          Register
        </button>
      </form>
    </>
  );
};
