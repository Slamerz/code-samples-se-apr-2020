import React from "react";
import { useSelector, useDispatch } from "react-redux";
import "./App.css";
import { actions } from "./store/actions/auth";
import { ShoppingList, Login } from "./components";

const LogoutButton = () => {
  const name = useSelector((state) => state.auth.name);
  const dispatch = useDispatch();
  return (
    <>
      <p>Welcome {name}!</p>
      <button onClick={() => dispatch(actions.logout())}>Log out bruh</button>
    </>
  );
};

function App() {
  const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);

  // const multipleStates = useSelector((state) => ({
  //   auth: state.auth,
  //   shoppingList: state.shoppingList,
  // }));

  return (
    <div className="App">
      {isLoggedIn ? (
        <>
          <LogoutButton />
          <br />
          <ShoppingList />
        </>
      ) : (
        <Login />
      )}
    </div>
  );
}

export default App;
