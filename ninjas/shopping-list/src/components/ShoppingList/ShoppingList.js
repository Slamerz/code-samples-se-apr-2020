import React, { useState } from "react";
import { v4 } from "uuid";
import { useSelector, useDispatch } from "react-redux";
import { actions } from "../../store/actions/shoppingList";

const ShoppingListItem = ({ id }) => {
  const item = useSelector((state) => state.shoppingList.items[id]);
  return (
    <li>
      {item.name} is a {item.category} product
    </li>
  );
};

const AddShoppingItem = () => {
  const [itemName, setItemName] = useState("");
  const [itemCategory, setItemCategory] = useState("veggies");
  const isLoading = useSelector((state) => state.shoppingList.loading);
  const dispatch = useDispatch();

  const addItem = (ev) => {
    ev.preventDefault();
    dispatch(
      actions.addItemToCart({
        name: itemName,
        id: v4(),
        category: itemCategory,
      }),
    );
    setItemName("");
    setItemCategory("veggies");
  };

  const onItemChange = (ev) => setItemName(ev.target.value);
  const onCategoryChange = (ev) => setItemCategory(ev.target.value);

  // function onItemChange(ev) {
  //   setItemName(ev.target.value);
  // }

  // function onCategoryChange(ev) {
  //   setItemCategory(ev.target.value);
  // }
  return (
    <>
      {isLoading ? (
        <p>Store is busy..</p>
      ) : (
        <form onSubmit={addItem}>
          <input value={itemName} onChange={onItemChange} />
          <select value={itemCategory} onChange={onCategoryChange}>
            <option value="dairy">Dairy</option>
            <option value="veggies">Veggies</option>
            <option value="snacks">Snack</option>
          </select>
          <button type="submit">Add</button>
        </form>
      )}
    </>
  );
};

const FilterShoppingList = () => {
  const currentFilter = useSelector((state) => state.shoppingList.currentFilter);
  const dispatch = useDispatch();

  return (
    <>
      <select value={currentFilter} onChange={(e) => dispatch(actions.changeFilter(e.target.value))}>
        <option value="dairy">Dairy</option>
        <option value="veggies">Veggies</option>
        <option value="snacks">Snack</option>
      </select>
    </>
  );
};

export const ShoppingList = () => {
  const shoppingList = useSelector((state) => state.shoppingList.items);
  const currentFilter = useSelector((state) => state.shoppingList.currentFilter);

  return (
    <>
      <h1>Shopping List</h1>
      <AddShoppingItem />
      <br />
      <br />
      <br />
      <br />
      <hr />
      <FilterShoppingList />
      <ol>
        {Object.values(shoppingList)
          .filter((item) => {
            return !currentFilter ? true : item.category === currentFilter;
          })
          .map(({ id }) => (
            <ShoppingListItem key={id} id={id} />
          ))}
      </ol>
    </>
  );
};

export default ShoppingList;
