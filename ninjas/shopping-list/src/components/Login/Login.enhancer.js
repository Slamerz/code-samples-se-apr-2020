import { connect } from "react-redux";
import { actions } from "../../store/actions/auth";

export default connect(null, {
  login: actions.login,
});
