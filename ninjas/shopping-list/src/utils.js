export const sleep = (ms = 2) => new Promise((resolve) => setTimeout(resolve, ms * 1000));
