import { combineReducers } from "redux";
import authReducer from "./authReducer";
import shoppingListReducer from "./shoppingListReducer";

const reducer = combineReducers({
  auth: authReducer,
  shoppingList: shoppingListReducer,
});

export default reducer;
