import { AUTH_LOGIN, AUTH_LOGOUT } from "../actions/auth";

const initialState = {
  isLoggedIn: false,
  name: "",
};

const authReducer = (state = { ...initialState }, { type, payload }) => {
  switch (type) {
    case AUTH_LOGIN:
      return {
        ...state,
        isLoggedIn: true,
        name: payload,
      };

    case AUTH_LOGOUT:
      console.log("huh....");
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

export default authReducer;
